class WeatherService{

    baseurl = "http://api.openweathermap.org/data/2.5/forecast?q="
    key = "&appid=f44e947afc6c0878ed4463a841db0599"
    options = "&units=metric&lang=fr"
    headers= {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    async find(city){
        return fetch(this.baseurl+city+this.key+this.options)
        .then(res => res.json())
        .catch(err =>console.error(err))
    }
}

export const weatherService = Object.freeze(new WeatherService());