import React, { useState } from 'react'
import { weatherService } from '../services/WeatherService';
import { FormWeather } from './FormWeather'
import { Weather } from './Weather'

interface Ville{
    name: string
}

export const LayoutWeather = () =>{
    const [forecast, setforecast] = useState({list:null});
    const [ville, setville] = useState({});

    const loadCity = (ville:Ville) =>{
        weatherService.find(ville.name).then(setforecast);
    }

    //{ forecast != {} ? <Weather forecast={forecast}/> : <></> }
    return <>
        <FormWeather onFinish={loadCity}/>
        {forecast.list && <Weather forecast={forecast}/>}
    </>
}