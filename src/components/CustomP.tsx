import React, { useState } from 'react'

interface PObject{
    titre: string,
    content: string
}

export const CustomP = (props:PObject) => {

    return <>
        <p>{props.titre} : {props.content}</p>
    </>

}