import React, { useState } from 'react'

export const CustomInput = (props:any) => {

    return <>
        <p>{props.name}</p>
        <input type={props.type} value={props.value} onChange={(event)=>props.onChange(event.target.value)} />
    </>

}