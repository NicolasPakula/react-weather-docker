import React from 'react'
import { CustomP } from './CustomP';
import { WeatherType } from './Weather'

interface KeyWeather{
    key: number,
    weather: WeatherType
}

export const WeatherTile = (props:KeyWeather) =>{

    const imgSrc = () =>{
        return "http://openweathermap.org/img/wn/"+props.weather.weather[0].icon+"@2x.png";
    } 

    return <div>
        <img src={imgSrc()}/>
        <p>{props.weather.weather[0].description}</p>
        <CustomP titre="Date" content={props.weather.dt_txt}/>
        <CustomP titre="Température ressentie" content={props.weather.main.feels_like}/>
        <CustomP titre="Température min" content={props.weather.main.temp_min}/>
        <CustomP titre="Température max" content={props.weather.main.temp_max}/>
        <CustomP titre="Sens du vent en degré" content={props.weather.wind.deg}/>
        <CustomP titre="Vitesse du vent" content={props.weather.wind.speed}/>

    </div>
}