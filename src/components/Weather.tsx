import React from 'react'
import { WeatherTile } from './WeatherTile'
import "./style.css"

export interface WeatherType{
    dt: number,
    main: any,
    wind: any,
    dt_txt: string,
    weather: any
}

export const Weather = (props:any) =>{
    return <div className="box">
        {props.forecast.list.map((weather:WeatherType, index:number) => {
            return <WeatherTile key={index} weather={weather} />
        })}
    </div>
}