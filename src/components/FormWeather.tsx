import React, { useState } from 'react'
import { CustomInput } from './CustomInput';

export const FormWeather = (props:any) =>{

    const [ville, setville] = useState("");

    const onSubmitForm = () => {
        props.onFinish(
            {
                name: ville
            }
        )
    };

    return <div>
        <p>Entrer le nom d'une ville</p>
        <CustomInput name="Ville" type="text" value={ville} onChange={setville}/>
        <button onClick={onSubmitForm}>Click Me!!</button>
    </div>
}

