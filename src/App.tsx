import React from 'react';
import logo from './logo.svg';
import './App.css';
import { LayoutWeather } from './components/LayoutWeather';

function App() {
  return (
    <div className="App">
      <LayoutWeather/>
    </div>
  );
}

export default App;
